===================
SQL CRUD OPERATIONS
===================

1. (CREATE) Adding a RECORD
	Terminal
	Adding a Record:
	Syntax:
		INSERT INTO table_name (column_name) VALUES (values1);

	Example:

		INSERT INTO artists (artistName) VALUES ("Nirvana");

		-- Mini Activity --

		INSERT INTO artists (artistName) VALUES ("Arthur Nery");

		INSERT INTO artists (artistName) VALUES ("Zack Tabudlo");

2. (READ) Show all records
	Terminal
		Displaying  / retrieving records
		Syntax:
			SELECT column_name FROM table_name;

		Example:
			SELECT artistName FROM artists;

3. (CREATE) Adding columns with multiple columns
	Terminal 
		Adding a record with multiple columns
		Syntax:
			INSERT INTO table_name (column_name, column_name) VALUES (value1, value2)

		Example:
			INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Nevermind", "1991-09-24", 1);

			-- Mini activity --

			INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Letters Never Sent", "2019-10-25", 2);

			INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Episode", "2021-10-15", 3);

4. (CREATE) Adding multiple records
	Terminal
		Adding multiple records
		Syntax:
			Insert Into table_name (column_name, column_name) VALUES (value1, value2), (value3, value4);

		Example:
			INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Smell Like A Teen Spirit", 501, "Grunge", 1);

			-- Mini Activity --

			INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Pagsamo", 502, "RNB/Soul", 2);

			INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Pano", 436, "RNB-Pop Ballad", 3);

5. Show records with selected columns
	Terminal
		Retrieving records with selected columns
		Syntax:
			SELECT (column_name1, column_name2) FROM table_name;

		Example:
			SELECT song_name FROM songs;
			SELECT song_name, genre FROM songs;

			-- Mini Activity --

			SELECT album_title, date_released FROM albums;

			-- will show the full table --
			SELECT * FROM songs;
			SELECT * FROM albums;
			SELECT * FROM artists;

6. Show records that meet certain condition
	Terminal
		Retrieving records with certain condition
		Syntax:
			SELECT column_name FROM table_name WHERE condition;

		Example:
			SELECT song_name FROM songs WHERE genre = "RNB/Soul";
			SELECT song_name FROM songs WHERE genre = "RNB"; -- empty --

7. Show records with multiple conditions.
	Terminal
		Displaying / retrieving records with multiple conditions
		Syntax:
			AND Clause
			SELECT column_name FROM table_name WHERE condition1 AND condition2;

			OR Clause
			SELECT column_name FROM table_name WHERE condition1 OR condition2;

			-- We can use AND and OR keyword for multiple expressions in the WHERE clause

		Example:
		-- at least 1 condition was met --
			SELECT song_name, length FROM songs WHERE length > 314 OR genre = "Grunge";
		-- all conditions must be met
			SELECT song_name, length FROM songs WHERE length > 314 AND genre = "Grunge";
		-- 
			SELECT * FROM songs WHERE length > 350 or genre = "Grunge";

8. (UPDATE) Updating Records
	Terminal

	Add a record to update:
		INSERT INTO artists (artistName) VALUES ("Incubus");
		INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Monuments and Melodies", "2009-06-16", 4) ;
		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Megalomaniac", 410, "Classical", 4);

	Updating Records:
		Syntax:
			UPDATE table_name SET column_name = value WHERE condition;
			UPDATE table_name SET column_name = value, column_name2 = value2 WHERE condition;
			-- When updating or deleting, add a WHERe clause or else you may update or delete all itmes in a table.
			UPDATE songs SET genre = "Rock" WHERE length > 400;
			UPDATE songs SET length = 240 WHERE song_name = "Pano";

9. (DELETE) Deleting records
	Terminal
		Deleting records
		Syntax
			DELETE FROM table_name WHERE condition;

		Example:
			DELETE FROM songs WHERE genre = "Rock" AND length > 400;
			-- removing WHERE clause deletes all rows;
			DELETE FROM songs;


-- MINI ACTIVITY -- 
	

	INSERT INTO users (username, password) VALUES ("John Cruz", "john@mail.com");
	INSERT INTO users (username, password) VALUES ("Jane Cruz", "jane@mail.com");

	INSERT INTO tasks (status, name, user_id) VALUES ("Completed", "Cooking", 1);
	INSERT INTO tasks (status, name, user_id) VALUES ("Pending", "Washing Dishes", 2);
	UPDATE users SET password = "IamJohn" WHERE id = 1;
	UPDATE users SET password = "IamJane" WHERE id = 2;

	